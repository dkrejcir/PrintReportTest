﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintReportTest.Data
{
    public class DataProvider
    {
        public IList<Book> GetBooks()
        {
            return new List<Book>
            {
                new Book { Author = "Author1", Name = "Name1", Publisher = "Publisher1" },
                new Book { Author = "Author2", Name = "Name2", Publisher = "Publisher2" },
                new Book { Author = "Author3", Name = "Name3", Publisher = "Publisher3" },
                new Book { Author = "Author4", Name = "Name4", Publisher = "Publisher4" },
                new Book { Author = "Author5", Name = "Name5", Publisher = "Publisher5" },
                new Book { Author = "Author6", Name = "Name6", Publisher = "Publisher6" },
                new Book { Author = "Author7", Name = "Name7", Publisher = "Publisher7" },
                new Book { Author = "Author8", Name = "Name8", Publisher = "Publisher8" },
                new Book { Author = "Author9", Name = "Name9", Publisher = "Publisher9" },
                new Book { Author = "Author10", Name = "Name10", Publisher = "Publisher10" },
            };
        }
    }
}
