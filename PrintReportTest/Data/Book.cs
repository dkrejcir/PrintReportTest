﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.Utils.Serializing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintReportTest.Data
{
    [Serializable]
    [HighlightedClass]
    public class Book
    {

        [HighlightedMember]
        public string Author { get; set; }
        [HighlightedMember]
        public string Name { get; set; }
        [HighlightedMember]
        public string Publisher { get; set; }
    }
}
