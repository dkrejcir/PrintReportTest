﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintReportTest
{
    public static class Utils
    {
        public static XtraReport ApplyEvenPages(XtraReport xtraReport)
        {
            XtraReport xtraReportResult = new XtraReport();
            xtraReportResult.Pages.Clear();

            xtraReport.CreateDocument();

            int p = 0;
            for (int i = 0; i < xtraReport.Pages.Count; i++)
            {
                xtraReportResult.Pages.Insert(p, xtraReport.Pages[i]);
                p++;

                Page evenPage = CreateEvenPage(xtraReport);
                xtraReportResult.Pages.Insert(p, evenPage);
                p++;
            }

            xtraReportResult.PrintingSystem.ContinuousPageNumbering = true;
            return xtraReportResult;
        }

        private static Page CreateEvenPage(XtraReport mainReport)
        {
            XtraReport xtraReportEven = new Report2();
            xtraReportEven.PaperKind = mainReport.PaperKind;
            xtraReportEven.CreateDocument();
            return xtraReportEven.Pages.First;
        }
    }
}
