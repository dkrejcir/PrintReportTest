﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrintReportTest
{
    public partial class Form1 : Form
    {

        // Main scenario is to have report eg. invoice which will have in odd pages invoice content and in even pages repeating some invoice rules. 
        // We have to define rules separately from invoice report, because rules will be used for many reports.


        Data.DataProvider provider;

        public string CurrentDir
        {
            get { return System.IO.Path.GetDirectoryName(Application.ExecutablePath); }
        }

        public Form1()
        {
            InitializeComponent();
            provider = new Data.DataProvider();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Report1 report1 = new Report1();
            report1.DataSource = provider.GetBooks();

            /*** test using Link class ***/
            //LinkTest p = new LinkTest(new List<XtraReport> { report1, new Report2() });
            //p.CreateDocument();
            //richTextBox1.Rtf = ExportToRtf(p.Link);
            //p.ShowPreview();

            /*** PDF is created correctly, RTF is empty ***/
            XtraReport r = Utils.ApplyEvenPages(report1);

            /*** both PDF even RTF are created correctly ***/
            //XtraReport r = report1;


            string rtf = ExportToRtf(r);
            richTextBox1.Rtf = rtf;
            File.WriteAllText(Path.Combine(CurrentDir, "test.rtf"), rtf, new UTF8Encoding());

            MemoryStream ms = ExportToPdf(r);
            ms.Seek(0, SeekOrigin.Begin);
            using (FileStream file = new FileStream(Path.Combine(CurrentDir, "test.pdf"), FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[ms.Length];
                ms.Read(bytes, 0, (int)ms.Length);
                file.Write(bytes, 0, bytes.Length);
                ms.Close();
            }
        }

        private string ExportToRtf(Link link)
        {
            MemoryStream ms = new MemoryStream();
            RtfExportOptions rtfOptions = new RtfExportOptions();
            rtfOptions.ExportMode = RtfExportMode.SingleFile;
            rtfOptions.ExportPageBreaks = true;
            link.ExportToRtf(ms, rtfOptions);
            byte[] bytes = ms.GetBuffer();
            var result = System.Text.Encoding.UTF8.GetString(bytes);
            return result;
        }

        private string ExportToRtf(XtraReport report)
        {
            MemoryStream ms = new MemoryStream();
            RtfExportOptions rtfOptions = new RtfExportOptions();
            rtfOptions.ExportMode = RtfExportMode.SingleFile;
            rtfOptions.ExportPageBreaks = true;
            report.ExportToRtf(ms, rtfOptions);
            byte[] bytes = ms.GetBuffer();
            var result = System.Text.Encoding.UTF8.GetString(bytes);
            return result;
        }

        public static MemoryStream ExportToPdf(XtraReport xtraReport)
        {
            xtraReport.ShowPrintStatusDialog = false;
            xtraReport.ShowPreviewMarginLines = false;
            xtraReport.ShowPrintMarginsWarning = false;

            MemoryStream ms = new MemoryStream();
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;

            // Specify the quality of exported images.
            pdfOptions.ConvertImagesToJpeg = false;
            pdfOptions.ImageQuality = PdfJpegImageQuality.Medium;
            // Specify the PDF/A-compatibility.
            pdfOptions.PdfACompatibility = PdfACompatibility.PdfA2b;
            // Export to PDF/A
            xtraReport.ExportToPdf(ms, pdfOptions);
            return ms;
        }
    }
}
